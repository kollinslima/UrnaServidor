/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urnaservidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import candidatos.Candidato;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author afonso
 */
public class UrnaServidor {
    private final int port;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private HashMap<Integer, Candidato> candidatos;
    private HashMap<Integer,Integer> votos;
    
    public UrnaServidor(){
        this.port = 40102;
    }
    
    public final void loadMaps(){
        candidatos = new HashMap();
        votos = new HashMap();
        String lines[] = readFile("candidatos.dat",Charset.defaultCharset()).split("\n");
        for(String c: lines){
            String cd[] = c.split(";");
            int cod = Integer.parseInt(cd[2]);
            Candidato cand = new Candidato(cod, cd[0], cd[1], cd[3]);
            candidatos.put(cod,cand);
            votos.put(cod,0);
        }
        votos.put(0,0); //padrão nulo
        votos.put(-1,0); //padrão branco
    }
    
    static String readFile(String path, Charset encoding){
        byte[] encoded;
        try {
            encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, encoding);
        } catch (IOException ex) {
            Logger.getLogger(UrnaServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void redirectClient(Socket client){
        try{
            output = new ObjectOutputStream(client.getOutputStream());
            input = new ObjectInputStream(client.getInputStream());
            UrnaHandler uh = new UrnaHandler(input,output,candidatos,votos);
            new Thread(uh).start();
            System.out.println("Servidor - conexão redirecionada para o handler");
        }catch(IOException ex) {}
    }
    
    public void exec(){
        System.out.println("Servidor - iniciando LOG");
        loadMaps();
        try (ServerSocket server = new ServerSocket(port)) {
            System.out.println("Servidor - Porta "+port+" aberta!");

            Socket client;            
            while(true){
                client = server.accept();
                System.out.println("Servidor - Recebendo nova conexão de: " + client.getInetAddress().getHostAddress());
                redirectClient(client);
            }
        }catch(IOException e){}
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new UrnaServidor().exec();
    }
    
}
