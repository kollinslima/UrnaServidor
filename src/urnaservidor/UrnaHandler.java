/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urnaservidor;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import candidatos.Candidato;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author afonso
 */
public class UrnaHandler implements Runnable{
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private HashMap<Integer,Candidato> candidatos;
    private HashMap<Integer,Integer> votos;
    private LinkedList<Integer> historico;
    private int evolucao = 0;
    String comandos[];

    public UrnaHandler(ObjectInputStream input,ObjectOutputStream output, HashMap<Integer,Candidato> candidatos,HashMap<Integer,Integer> votos){
        this.output = output;
        this.input = input;
        this.candidatos = candidatos;
        this.votos = votos;
        this.historico = new LinkedList();
    }
    
    @Override
    public void run(){
        System.out.println("Handler - Nova instância criada!\n\t Iniciando LOG...");
        System.out.println("Handler - Enviando mapa de candidatos");
        enviaCandidatos();
        while(true){
            try {
                String comando = (String) input.readObject();
                processarCmd(comando);
            } catch (IOException|ClassNotFoundException ex) {
                System.out.println("Handler - Connexão encerrada");
                break;
            }
        }
        System.out.println("Handler - Handler encerrado");

    }
    
    private void processarCmd(String comando){
        comandos = comando.split(":");
        switch(comandos[0]){
            case("voto"):
                efetivarVoto();                        
            break;
            case("fechar"):
                System.out.println("Handler - Votação encerrada");
                encerrarConexao();
            break; 
            case("candidatos"):
                enviaCandidatos();
        }
    }
    
    private void enviaCandidatos() {
        try {
            output.writeObject(candidatos);
        } catch (IOException ex) {
            Logger.getLogger(UrnaHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void encerrarConexao(){
        try {
            input.close();
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(UrnaHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void efetivarVoto() {
        int numVotos = Integer.parseInt(comandos[2]);
        int codigoCandidato = Integer.parseInt(comandos[1]);
        String nome;
        if(numVotos == evolucao+1){
            Candidato c = candidatos.get(codigoCandidato);
            if(c == null){
                if(codigoCandidato == -1){
                    nome = "branco";
                }else{
                    nome = "nulo";
                    codigoCandidato = 0;
                }
            }else{
                nome = c.getNome_candidato();
            }
            System.out.println("Handler - Voto recebido: "+nome);
            contabilizarVoto(codigoCandidato,1);
            historico.add(codigoCandidato);
            evolucao++;
            atualizaUrna();
            System.out.println("Handler - Votação atual: "+votos);
        }else{
            System.out.println("Handler - Votos perdidos! Realizando recontagem...");
            recontagemVotos();
        }   
    }
    
    private synchronized void contabilizarVoto(int codigoCandidato,int adicao){
        int numVotos = votos.remove(codigoCandidato)+adicao;
        votos.put(codigoCandidato,numVotos);
        Candidato c = candidatos.get(codigoCandidato);
        
        if(c != null){
            c.setNum_votos(numVotos);
        }
    }
    
    private void atualizaUrna(){
        try {
            output.writeUnshared(votos);//Faz o envio do objeto atualizado
        } catch (IOException ex) {
            Logger.getLogger(UrnaHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void recontagemVotos() {
        try {
            output.writeObject(null);
            LinkedList<Integer> historicoUrna = (LinkedList<Integer>) input.readObject();
            //retirando os votos antigos
            for(int cod : historico.subList(0, historico.size())){
                contabilizarVoto(cod, -1);
            }
            //inserindo os novos votos
            for(int cod : historicoUrna.subList(0, historicoUrna.size())){
                contabilizarVoto(cod,1);
            }
            //substituindo históricos
            historico = historicoUrna;
        } catch (IOException|ClassNotFoundException ex) {
            Logger.getLogger(UrnaHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
