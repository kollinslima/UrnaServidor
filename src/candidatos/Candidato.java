/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidatos;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author kollins
 */
public class Candidato implements Serializable{
    private int codigo_votacao;
    private String nome_candidato;
    private String partido;
    private int num_votos;
    private ImageIcon img_candidato;
    
    public Candidato(){
        
    }
    
    public Candidato(int codigo, String nome, String partido, String dir){
        this.codigo_votacao = codigo;
        this.nome_candidato = nome;
        this.num_votos = 0;
        this.partido = partido;
        setImg_candidato(dir);
    }

    public int getCodigo_votacao() {
        return codigo_votacao;
    }
    
    public void setCodigo_votacao(int codigo_votacao) {
        this.codigo_votacao = codigo_votacao;
    }
    
    public String getNome_candidato() {
        return nome_candidato;
    }

    public void setNome_candidato(String nome_candidato) {
        this.nome_candidato = nome_candidato;
    }
    
    public String getPartido() {
        return partido;
    }
    
    public void setPartido(String partido) {
        this.partido = partido;
    }
    
    public int getNum_votos() {
        return num_votos;
    }
    
    public void setNum_votos(int num_votos) {
        this.num_votos = num_votos;
    }
    
    @Override
    public String toString(){
        return "Candidato: "+nome_candidato+" Partido: "+partido+" Código: "+codigo_votacao;
    }

    /**
     * @return the img_candidato
     */
    public ImageIcon getImg_candidato() {
        return img_candidato;
    }

    /**
     * @param dir the img_candidato to set
     */
    private void setImg_candidato(String dir) {
        try {
            URL c = new File(dir).toURI().toURL();
            this.img_candidato = new ImageIcon(c);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Candidato.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
